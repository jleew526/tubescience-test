import csv
from collections import OrderedDict

DESIRED_FIELDS = ["PROP_NAME", "ADDRESS", "CITY", "STATE_ID", "ZIP"]

def parse_file_into_dict(filename):
	with open(filename, mode='r') as csvfile:
	    reader = csv.reader(csvfile)
	    headers = next(reader, None)
	    col_rows = []
	    for row in reader:
	    	col_rows.append(OrderedDict(zip(headers, row)))
	    # filtered_rows = [row for row in col_rows if row.get('STATE_ID', '') == 'ca']

	   	filtered_rows = []
	    for row in col_rows:
	    	new_row = OrderedDict()
	    	if row.get('STATE_ID', '') == 'ca':
	    		has_count = 0
	    		lacks_count = 0
	    		missing_strs = []
	    		missing_fields = 0
	    		for field in row: 
	    			if row.get(field):
	    				if lacks_count:
	    					missing_strs.append(str(lacks_count))
	    					lacks_count = 0
	    				has_count = has_count +1
	    			else:
	    				if has_count:
	    					missing_strs.append(str(has_count))
	    					has_count = 0
	    				lacks_count = lacks_count +1
	    				missing_fields = missing_fields + 1
	    			if field in DESIRED_FIELDS:
		    			new_row[field] = row.get(field, '')
		    	if lacks_count:
		    		missing_strs.append(str(lacks_count))
		    	if has_count:
		    		missing_strs.append(str(has_count))
		    	new_row["MISSING_FIELD_COUNT"] = missing_fields
		    	new_row["MISSING_DATA_ENCODING"] = ''.join(missing_strs)
		    	new_row["MISSING_DATA_ENCODING_JASON"] = '-'.join(missing_strs)
		    	filtered_rows.append(new_row)

	    return filtered_rows