import React, { Component } from 'react';
import { connect } from "react-redux";
const mapStateToProps = state => {
  return { headers: state.headers,
           rows: state.rows };
};

const Td = ({key, col}) => (
	<td key={key}>{col}</td>
)

const Tr = ({key, row}) => (
	<tr key={key}>
		{row.map((el,idx) => (
	      <Td key={idx} col={el} />
	    ))}
	</tr>
)

const Table = ({ headers, rows }) => (
    <table className="pure-table">
	    <thead>
	        <tr>
	        	{headers.map(el => (
			      <th key={el}>
			        {el}
			      </th>
			    ))}
	        </tr>
	    </thead>

	    <tbody>
	    	{rows.map(el => (
			      <Tr key={el.PROP_NAME} row={el} />
			    ))}
	    	
	    </tbody>
	</table>
)
export default connect(mapStateToProps)(Table) 