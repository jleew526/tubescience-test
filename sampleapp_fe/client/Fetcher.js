import React, { Component } from 'react';
import axios from 'axios'
import Button from './FetcherButton';
import Table from './FetcherGrid';
import { connect } from "react-redux";
import { fetch_properties } from "../actions/index";


class Fetcher extends Component {
  constructor () {
    super()
    this.state = {
      headers: [],
      body: [],
    }

    
  }

  render() {
    return <div>
      <Button/>
      <Table/>
    </div>
  }
}

const mapStateToProps = state => ({
  ...state
});

const mapDispatchToProps = dispatch => ({
  fetch_properties: (payload) => dispatch(fetch_properties(payload))
});
export default  connect(mapStateToProps, mapDispatchToProps)(Fetcher);