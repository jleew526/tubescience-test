import React, { Component } from 'react';
import axios from 'axios';
import { connect } from "react-redux";
import { fetch_properties } from "../actions/index";
class Button extends Component {
  constructor () {
    super()
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick () {
    axios.get('http://localhost:9000/data')
      .then(response => this.props.fetch_properties(response.data))
  }

  render () {
    return (
      <div className='button__container'>
        <button className='button' onClick={this.handleClick}>Fetch California Listings</button>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  ...state
});

const mapDispatchToProps = dispatch => ({
  fetch_properties: (payload) => dispatch(fetch_properties(payload))
});
export default  connect(mapStateToProps, mapDispatchToProps)(Button);