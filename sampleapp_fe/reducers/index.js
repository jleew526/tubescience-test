import { FETCH_PROPERTIES } from "../constants/action-types";
const initialState = {
  headers: [],
  rows: []
};
function rootReducer(state = initialState, action) {
  if (action.type === FETCH_PROPERTIES) {
    const headers = [];
    const rows = [];

    if (action.payload.length == 0) {
      console.log("No results returned!");
    }

    for (var key in action.payload[0]) {
      headers.push(key);
    }

    action.payload.forEach(function (row) {
      const this_row = [];
      for (var val in row) {
        this_row.push(row[val]);
      }
      rows.push(this_row);
    });

    return Object.assign({}, state, {
      headers: headers,
      rows: rows
    });
  }
  return state;
}
export default rootReducer;