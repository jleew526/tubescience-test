import { FETCH_PROPERTIES } from "../constants/action-types";
export function fetch_properties(payload) {
  return { type: FETCH_PROPERTIES, payload };
}