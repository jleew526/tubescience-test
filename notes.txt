Notes:

Frontend: 
    Libraries added:
        axios: Added for HTTP requests
        babel-reset-stage0: Added to support spread operator
        css-loader/style-loader: Added to support stylesheet loading
        react-redux/redux: Added for state management
        
Backend:
    Modifications made:
        MISSING_DATA_ENCODING_JASON: The original MISSING_DATA_ENCODING field is not very clear about the actual number of present fields vs missing fields. For example, if there were 11 fields present, 1 missing, then 2 present, the string would be 1112, but it's hard to tell whether that is 11 present, 1 missing, 2 present, or 1 present, 11 missing, 2 present or 1 present, 1 missing, and 12 present. By seperating out the digits with a dash it makes it easier to figure out the actual present vs missing columns count.